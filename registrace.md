﻿[< Dokumentace](README.md)
## Jak se registrovat
Před tím, než se budete moci přihlásit na Avavu a začít pracovat, bude nutné se registrovat. Registrace probíhá přes školní Google účty na stánkách [Avavy](http://svs.gyarab.cz).
V levém panelu vyberte možnost "Registrace" a dále se řiďte pokyny na stránce.
Po úspěšné registraci se již můžete připojit přes SSH.