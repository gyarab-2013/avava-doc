﻿[< Dokumentace](README.md)
# AVAVA Dokumentace

Toto je dokumentace ke studentskému vývojovému serveru AVAVA.

## Sections
- [Registrace](registrace.md)
- [SSH / Vzdálený přístup](ssh.md)
- [SFTP / Přenos souborů](sftp.md)